﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GoogleRobot.aspx.cs" Inherits="GoogleRobot" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <title>reCAPTCHA demo: Explicit render for multiple widgets</title>
    <script type="text/javascript">
        var verifyCallback = function (response) {
            alert(response);
        };
        var widgetId1;
        var widgetId2;
        var onloadCallback = function () {
            // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
            // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
            widgetId1 = grecaptcha.render('example1', {
                '6Lf5cCITAAAAAGHeQaUlq5UrJVF1jeTFA0LEOArn': '6Lf5cCITAAAAACnusCRm9jxe9dMpuddXjZTl8mzn',
                'theme': 'light'
            });
            widgetId2 = grecaptcha.render(document.getElementById('example2'), {
                '6Lf5cCITAAAAAGHeQaUlq5UrJVF1jeTFA0LEOArn': '6Lf5cCITAAAAACnusCRm9jxe9dMpuddXjZTl8mzn'
            });
            grecaptcha.render('example3', {
                '6Lf5cCITAAAAAGHeQaUlq5UrJVF1jeTFA0LEOArn': '6Lf5cCITAAAAACnusCRm9jxe9dMpuddXjZTl8mzn',
                'callback': verifyCallback,
                'theme': 'dark'
            });
        };
    </script>
</head>
<body>
   <!-- The g-recaptcha-response string displays in an alert message upon submit. -->
    <form action="javascript:alert(grecaptcha.getResponse(widgetId1));">
      <div id="example1"></div>
      <br>
      <input type="submit" value="getResponse">
    </form>
    <br>
    <!-- Resets reCAPTCHA widgetId2 upon submit. -->
    <form action="javascript:grecaptcha.reset(widgetId2);">
      <div id="example2"></div>
      <br>
      <input type="submit" value="reset">
    </form>
    <br>
    <!-- POSTs back to the page's URL upon submit with a g-recaptcha-response POST parameter. -->
    <form action="?" method="POST">
      <div id="example3"></div>
      <br>
      <input type="submit" value="Submit">
    </form>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>
</body>
</html>
