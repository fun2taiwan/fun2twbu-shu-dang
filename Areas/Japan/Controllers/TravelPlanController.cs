﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2tw.Controllers;
using Fun2tw.Models;
using Fun2twModels.Models;
using MvcPaging;
using Newtonsoft.Json;

namespace Fun2tw.Areas.Japan.Controllers
{
    public class TravelPlanController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 16;
        //
        // GET: /TravelPlanController/

        public ActionResult Index(int? page, FormCollection fc,int id=0)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);

            var travelplans = _db.TravelPlans.Where(x => x.CategoryId == 2 || x.CategoryId == 7).OrderByDescending(p => p.InitDate).AsQueryable();
            ViewBag.CategoryId = new SelectList(_db.TravelPlanCatalogs.Where(x => x.id == 2 || x.id == 7).OrderBy(p => p.InitDate), "id", "Subject");
            
            if (hasViewData("SearchBySubject"))
            {
                string searchBySubject = getViewDateStr("SearchBySubject");
                travelplans = travelplans.Where(w => w.Subject.Contains(searchBySubject));
            }
            if (id != 0)
            {
                switch (id)
                {
                    case 1:
                        travelplans = travelplans.Where(w => w.TagInfo.Contains("枋山鄉"));
                        ViewBag.URLCategory = "枋山鄉";
                        break;
                    case 2:
                        travelplans = travelplans.Where(w => w.TagInfo.Contains("獅子鄉"));
                        ViewBag.URLCategory = "獅子鄉";
                        break;
                    case 3:
                        travelplans = travelplans.Where(w => w.TagInfo.Contains("滿州鄉"));
                        ViewBag.URLCategory = "滿州鄉";
                        break;
                    case 4:
                        travelplans = travelplans.Where(w => w.TagInfo.Contains("牡丹鄉"));
                        ViewBag.URLCategory = "牡丹鄉";
                        break;
                    case 5:
                        travelplans = travelplans.Where(w => w.TagInfo.Contains("恆春鎮"));
                        ViewBag.URLCategory = "恆春鎮";
                        break;
                    case 6:
                        travelplans = travelplans.Where(w => w.TagInfo.Contains("車城鄉"));
                        ViewBag.URLCategory = "車城鄉";
                        break;
                    case 7:
                        travelplans = travelplans.Where(w => w.TagInfo.Contains("生態"));
                        ViewBag.URLCategory = "生態之旅";
                        break;
                    case 8:
                        travelplans = travelplans.Where(w => w.TagInfo.Contains("親水"));
                        ViewBag.URLCategory = "親水之旅";
                        break;
                    case 9:
                        travelplans = travelplans.Where(w => w.TagInfo.Contains("休閒遊憩之旅"));
                        ViewBag.URLCategory = "休閒遊憩之旅";
                        break;
                    case 10:
                        travelplans = travelplans.Where(w => w.TagInfo.Contains("古蹟"));
                        ViewBag.URLCategory = "古蹟文化之旅";
                        break;
                    case 11:
                        travelplans = travelplans.Where(w => w.TagInfo.Contains("景觀之旅"));
                        ViewBag.URLCategory = "景觀之旅";
                        break;
                    case 12:
                        travelplans = travelplans.Where(w => w.TagInfo.Contains("溫泉"));
                        ViewBag.URLCategory = "溫泉其他之旅";
                        break;
                }
            }

            //            ViewBag.Subject = Subject;
            return View(travelplans.Where(x=>x.TravelPlanImages.Count>0).OrderByDescending(p => p.TravelPlanImages.Count).ToPagedList(currentPageIndex, DefaultPageSize));
        }

        public ActionResult Search(int? page, FormCollection fc, string[] Category = null, string[] Town = null,int index=0)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);


            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);

            var travelplans = _db.TravelPlans.OrderByDescending(p => p.InitDate).AsQueryable();

            //if (hasViewData("SearchBySubject"))
            //{
            //    string searchBySubject = getViewDateStr("SearchBySubject");
            //    shopinfos = shopinfos.Where(w => w.Subject.Contains(searchBySubject));
            //}
            string[] Towns = new string[] { };
            if (Town != null)
            {
                Towns = Town;
            }
            else if (Session["travelplansTown"] != null)
            {
                Towns = (string[])Session["travelplansTown"];
            }
            string[] Categorys = new string[] { };
            if (Category != null)
            {
                Categorys = Category;
            }
            else if (Session["travelplansCategory"] != null)
            {
                Categorys = (string[])Session["travelplansCategory"];
            }

            if (Towns.Length > 0)
            {
                travelplans = travelplans.Where(x => x.CategoryId == 0);
                foreach (string item in Towns)
                {
                    //Linq或的語法
                    travelplans = travelplans.Concat(_db.TravelPlans.Where(x => x.TagInfo.Contains(item)));
                }
            }
            var travelplans1 = _db.TravelPlans.Where(x => x.CategoryId == 0).OrderByDescending(p => p.InitDate).AsQueryable();
            if (Categorys.Length > 0)
            {
                foreach (string item in Categorys)
                {
                    //Linq或的語法
                    travelplans1 = travelplans1.Concat(_db.TravelPlans.Where(x => x.TagInfo.Contains(item)));
                }
            }
            if (travelplans1.Any())
            {
                travelplans = travelplans.Intersect(travelplans1);
            }

            travelplans = travelplans.Distinct();


            ViewBag.ClassId = _db.TravelPlanCatalogTags.OrderBy(x => x.ListNum).ToList();
            ViewBag.selectClassId = Categorys;
            ViewBag.selectTown = Towns;
            Session["travelplansCategory"] = Categorys;
            Session["travelplansTown"] = Towns;


            //            ViewBag.Subject = Subject;
            return View(travelplans.Where(x => x.TravelPlanImages.Count > 0).OrderByDescending(p => p.TravelPlanImages.Count).ToPagedList(currentPageIndex, DefaultPageSize));
        }
      
        [HttpPost]
        public ActionResult Search(int? page, FormCollection fc, string[] Category = null, string[] Town = null)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);


            //取得正確的頁面
            int currentPageIndex =1;

            var travelplans = _db.TravelPlans.OrderByDescending(p => p.InitDate).AsQueryable();

            //if (hasViewData("SearchBySubject"))
            //{
            //    string searchBySubject = getViewDateStr("SearchBySubject");
            //    shopinfos = shopinfos.Where(w => w.Subject.Contains(searchBySubject));
            //}
            string[] Towns = new string[] { };
            if (Town != null)
            {
                Towns = Town;
            }
           
            string[] Categorys = new string[] { };
            if (Category != null)
            {
                Categorys = Category;
            }
           

            if (Towns.Length > 0)
            {
                travelplans = travelplans.Where(x => x.CategoryId == 0);
                foreach (string item in Towns)
                {
                   
                    //Linq或的語法
                    travelplans = travelplans.Concat(_db.TravelPlans.Where(x => x.TagInfo.Contains(item)));
                }
            }
            var travelplans1 = _db.TravelPlans.Where(x => x.CategoryId == 0).OrderByDescending(p => p.InitDate).AsQueryable();
            if (Categorys.Length > 0)
            {

                foreach (string item in Categorys)
                {
                    //Linq或的語法
                    travelplans1 = travelplans1.Concat(_db.TravelPlans.Where(x => x.TagInfo.Contains(item)));
                }
            }
            if (travelplans1.Any())
            {
                travelplans = travelplans.Intersect(travelplans1);
            }

            travelplans = travelplans.Distinct();


            ViewBag.ClassId = _db.TravelPlanCatalogTags.OrderBy(x => x.ListNum).ToList();
            ViewBag.selectClassId = Categorys;
            ViewBag.selectTown = Towns;
            Session["travelplansCategory"] = Categorys;
            Session["travelplansTown"] = Towns;


            //            ViewBag.Subject = Subject;
            //return View(travelplans.Where(x => x.TravelPlanImages.Count > 0).OrderByDescending(p => p.TravelPlanImages.Count).ToPagedList(currentPageIndex, DefaultPageSize));
            return RedirectToAction("Search");
        }

        public ActionResult Details(int id = 0)
        {
            TravelPlan travelPlan = _db.TravelPlans.Find(id);
            if (travelPlan == null)
            {
                //return HttpNotFound();
                return View();
            }
            if (Session["history"] != null)
            {
                List<TravelPlan> travelPlans = (List<TravelPlan>) Session["history"];
                if (travelPlans.All(x => x.Id != id))
                {
                    travelPlans.Add(travelPlan);
                }
                Session["history"] = travelPlans;

            }
            else
            {
                List<TravelPlan> travelPlans = new List<TravelPlan>();
                if (travelPlans.All(x => x.Id != id))
                {
                    travelPlans.Add(travelPlan);
                }
                Session["history"] = travelPlans;
            }
            int townID = 1;
            switch (travelPlan.Town)
            {
                case "枋山鄉":
                    townID = 1;
                    break;
                case "獅子鄉":
                    townID = 2;
                    break;
                case "滿州鄉":
                    townID = 3;
                    break;
                case "牡丹鄉":
                    townID = 4;
                    break;
                case "恆春鎮":
                    townID = 5;
                    break;
                case "車城鄉":
                    townID = 6;
                    break;
            }
            ViewBag.townID = townID;
           
            return View(travelPlan);
        }

        public ActionResult Json(int id = 0)
        {

            string jsonContent;
            //var travelplans = _db.TravelPlans.Where(x => (x.CategoryId == 2 || x.CategoryId == 7) && x.TravelPlanImages.Count > 0).OrderByDescending(p => p.TravelPlanImages.Count).Select(
            //        c => new
            //        {
            //            Id = c.Id,
            //            Catalog = c.Catalog.Subject,
            //            Subject = c.Subject,
            //            UpImageUrl = c.TravelPlanImages.Count > 0 ? "https://www.fun2tw.com/upfiles/" + c.TravelPlanImages.FirstOrDefault().Folder + c.TravelPlanImages.FirstOrDefault().FileName : "",
            //            InitDate = c.InitDate
            //        });
            var travelplans = _db.TravelPlans.Where(x => (x.CategoryId == 7) && x.TravelPlanImages.Count > 0).OrderByDescending(p => p.TravelPlanImages.Count).Select(
                   c => new
                   {
                       Id = c.Id,
                       Catalog = c.Catalog.Subject,
                       Subject = c.Subject,
                       Lat = c.Lat,
                       Lon = c.Lon,
                       Address = c.Subject,
                        Tel = c.Tel ?? "",
                       UpImageUrl = c.TravelPlanImages.Count > 0 ? "https://www.fun2tw.com/upfiles/" + c.TravelPlanImages.FirstOrDefault().Folder + c.TravelPlanImages.FirstOrDefault().FileName : "",
                       InitDate = c.InitDate,
                      DisShow="請開啟GPS",
                       Type = "Travel",
                   });
            jsonContent = JsonConvert.SerializeObject(travelplans, Formatting.Indented);



            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }

        public ActionResult GetJsonbyLocation(string lon, string lat)
        {
            //int dis = 300000;             //以公尺為單位
            double Lon = 0;
            double Lat = 0;

            if (double.TryParse(lon, out Lon))
            {
                Lon = double.Parse(lon);
                if (Lon < -180 || Lon > 180)
                {
                    return Content("參數lon：位置的經度。 這個值範圍可以從 -180.0 到 180.0。");
                }
            }
            if (double.TryParse(lat, out Lat))
            {
                Lat = double.Parse(lat);
                if (Lat < -90 || Lat > 90)
                {
                    return Content("參數lat：位置的緯度。 這個值範圍可以從 -90.0 到 90.0。");
                }
            }

            if (Lon == 0 && Lat == 0)
            {
                return HttpNotFound();
            }


            var travelplans = _db.TravelPlans.Where(x => (x.CategoryId == 7) && x.TravelPlanImages.Count > 0).ToList();
            var coord = new GeoCoordinate(Lat, Lon);
            var jsonitem = travelplans.Where(x => !string.IsNullOrEmpty(x.Lat) && !string.IsNullOrEmpty(x.Lon)
                && double.Parse(x.Lat) <= 90 && double.Parse(x.Lat) >= -90
                && double.Parse(x.Lon) <= 180 && double.Parse(x.Lon) >= -180
                ).Select(
                    c => new MapItem
                    {
                        Id = c.Id.ToString(),
                        Subject = c.Subject,
                        UpImageUrl = c.TravelPlanImages.Count > 0 ? "https://www.fun2tw.com/upfiles/" + c.TravelPlanImages.FirstOrDefault().Folder + c.TravelPlanImages.FirstOrDefault().FileName : "",
                        Lat = c.Lat,
                        Lon = c.Lon,
                        Dis = (int)(new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord),
                        DisShow = (new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord) > 1000 ? string.Format("{0:0.0}公里", (new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord) / 1000) : "附近",
                        Type = "Travel",
                        Address = c.Subject,
                        Tel = c.Tel
                    }
                )
                .OrderBy(o => o.Dis)
                .ToList();

            //List<ScheduleModel.Models.Health> item2 = db2.Healths.ToList();
            //var jsonitem2 = item2.Where(x => !string.IsNullOrEmpty(x.Lat) && !string.IsNullOrEmpty(x.Lon)
            //    && double.Parse(x.Lat) <= 90 && double.Parse(x.Lat) >= -90
            //    && double.Parse(x.Lon) <= 180 && double.Parse(x.Lon) >= -180
            //    && (new GeoCoordinate(double.Parse(x.Lat), double.Parse(x.Lon))).GetDistanceTo(coord) <= dis).Select(
            //         c => new MapItem
            //         {
            //             Id = c.Id.ToString(),
            //             Subject = c.Subject,
            //             Lat = c.Lat,
            //             Lon = c.Lon,
            //             Dis = (int)((new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord)),
            //             DisShow = (new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord) > 1000 ? string.Format("{0:0.0}公里", (new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord) / 1000) : string.Format("{0}公尺", (int)(new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord)),
            //             Type = GetCategoryName(c.Category),
            //             Address = c.Address
            //         }
            //    )
            //    .OrderBy(o => o.Dis)
            //    .ToList();

            var jsonitem3 = jsonitem.OrderBy(o => o.Dis);

            string jsonContent = JsonConvert.SerializeObject(jsonitem3, Formatting.Indented);
            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }

        public ActionResult OverView()
        {
            ViewBag.ClassId = _db.TravelPlanCatalogTags.OrderBy(x => x.ListNum).ToList();
            return View();
        }

        private string GetImage(TravelPlan travelPlan)
        {
            if (travelPlan.TravelPlanImages.Count > 0)
            {
                var img = travelPlan.TravelPlanImages.FirstOrDefault();
                return string.Format("/upfiles/{0}{1}", img.Folder, img.FileName);
            }
            else
            {
                return "";
            }
        }
    }
}
