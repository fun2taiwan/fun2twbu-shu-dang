﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.EnterpriseServices.Internal;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Fun2tw.Controllers;
using Fun2tw.Models;
using Fun2twModels.Models;
using HppApi;
using MvcPaging;
using Newtonsoft.Json;
using Transaction = Fun2twModels.Models.Transaction;


namespace Fun2tw.Areas.Japan.Controllers
{
    public class TravelSchduleController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 20;
        //
        // GET: /TravelSchdule/

        public ActionResult ThemeJson()
        {
            var themes = _db.TravelThemes.Where(x => x.TravelProducts.Any(y => y.Enable == BooleanType.是)).ToList();
            foreach (TravelTheme theme in themes)
            {
                theme.UpImageUrl = "https://www.fun2tw.com/upfiles/images/" + theme.UpImageUrl;
            }
            string jsonContent = JsonConvert.SerializeObject(themes, Formatting.Indented);
            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }



        public ActionResult ListByThemeJson(int id = 0)
        {

            string jsonContent;
            if (id > 0)
            {
                var products = _db.TravelProducts.Where(x => x.ThemeId == id && x.Enable == BooleanType.是).Select(
                    c => new
                    {
                        Id = c.Id,
                        City = c.TravelCity.Subject,
                        Theme = c.TravelTheme.Subject,
                        Subject = c.Subject,
                        UpImageUrl = "https://www.fun2tw.com/upfiles/images/" + c.UpImageUrl,
                        InitDate = c.InitDate
                    });
                jsonContent = JsonConvert.SerializeObject(products, Formatting.Indented);
            }
            else
            {
                var products = _db.TravelProducts.Where(x => x.Enable == BooleanType.是).Select(
                    c => new
                    {
                        Id = c.Id,
                        City = c.TravelCity.Subject,
                        Theme = c.TravelTheme.Subject,
                        Subject = c.Subject,
                        UpImageUrl = "https://www.fun2tw.com/upfiles/images/" + c.UpImageUrl,
                        InitDate = c.InitDate
                    });
                jsonContent = JsonConvert.SerializeObject(products, Formatting.Indented);
            }



            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }

        public ActionResult Index(int? ThemeId, int? page, FormCollection fc)
        {

            //取得正確的頁面
            int currentPageIndex = 0;




            if (ThemeId.HasValue && ThemeId != 0)
            {

                fc["SearchByThemeId"] = ThemeId.ToString();
                //記住搜尋條件
                GetCatcheRoutes(page, fc, null, false);
                //取得正確的頁面
                currentPageIndex = getCurrentPage(page, fc, null, false);
            }
            else
            {
                //記住搜尋條件
                GetCatcheRoutes(page, fc);
                //取得正確的頁面
                currentPageIndex = getCurrentPage(page, fc);
            }

            var travelproducts = _db.TravelProducts.Include(t => t.TravelCity).Include(t => t.TravelTheme).Where(x => x.Enable == BooleanType.是 && x.UpImageUrl != null).OrderByDescending(p => p.InitDate).AsQueryable();
            ViewBag.CityId = new SelectList(_db.TravelCities.OrderBy(p => p.InitDate), "Id", "Subject");
            ViewBag.ThemeId = new SelectList(_db.TravelThemes.Where(x => x.TravelProducts.Any(y => y.Enable == BooleanType.是)).OrderBy(p => p.InitDate), "Id", "Subject");

            if (hasViewData("SearchByThemeId"))
            {
                int searchByThemeId = getViewDateInt("SearchByThemeId");
                travelproducts = travelproducts.Where(w => w.ThemeId == searchByThemeId);
            }

            //            ViewBag.Subject = Subject;
            return View(travelproducts.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));
        }





        //
        // GET: /TravelProduct/Details/5

        public ActionResult Details(int id = 0)
        {
            TravelProduct travelproduct = _db.TravelProducts.Find(id);
            if (travelproduct == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(travelproduct);
        }

        [Authorize]
        public ActionResult SelectDate(int id = 0)
        {
            TravelProduct travelproduct = _db.TravelProducts.Find(id);
            if (travelproduct == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(travelproduct);
        }
        [HttpPost]
        public ActionResult SelectDate(DateTime? selectDate, int? SinglePrice, int? DoublePrice, int? FourPrice, int? SixPrice, int? EightPrice, int? WithoutBedPrice, int? BabyPric, int? OneDayAdult, int? OneDayChild, int? OneDayBaby, string Coupon, string Mobile, int id = 0)
        {
            

            TravelProduct travelproduct = _db.TravelProducts.Find(id);
            if (travelproduct == null)
            {
                return HttpNotFound();
            }
            if (!selectDate.HasValue)
            {
                ViewBag.Message = "請選擇出發日期";
                return View(travelproduct);
            }
            if (!string.IsNullOrEmpty(Coupon))
            {
                var eCoupon =
                    _db.TravelCoupons.FirstOrDefault(x => x.CouponCode == Coupon && x.IsUse == BooleanType.否 && x.EndDate>DateTime.Now);
                if (eCoupon == null)
                {
                    ViewBag.Message = "Coupon Code 無效!";
                    return View(travelproduct);
                }
                Session["Coupon"] = Coupon;

            }

            List<TravelOrderDetail> travelOrderDetails = new List<TravelOrderDetail>();
            if (SinglePrice.HasValue && SinglePrice > 0)
            {
                for (int i = 1; i <= SinglePrice; i++)
                {
                    TravelOrderDetail detail = new TravelOrderDetail();
                    detail.Subject = "單人房";
                    detail.Price = (int)travelproduct.SinglePrice;
                    travelOrderDetails.Add(detail);
                }


            }

            if (DoublePrice.HasValue && DoublePrice > 0)
            {
                for (int i = 1; i <= DoublePrice; i++)
                {
                    TravelOrderDetail detail = new TravelOrderDetail();
                    detail.Subject = "雙人房";
                    detail.Price = (int)travelproduct.DoublePrice;
                    travelOrderDetails.Add(detail);
                }


            }

            if (FourPrice.HasValue && FourPrice > 0)
            {
                for (int i = 1; i <= FourPrice; i++)
                {
                    TravelOrderDetail detail = new TravelOrderDetail();
                    detail.Subject = "四人房";
                    detail.Price = (int)travelproduct.FourPrice;
                    travelOrderDetails.Add(detail);
                }


            }

            if (SixPrice.HasValue && SixPrice > 0)
            {
                for (int i = 1; i <= SixPrice; i++)
                {
                    TravelOrderDetail detail = new TravelOrderDetail();
                    detail.Subject = "六人房";
                    detail.Price = (int)travelproduct.SixPrice;
                    travelOrderDetails.Add(detail);
                }


            }

            if (EightPrice.HasValue && EightPrice > 0)
            {
                for (int i = 1; i <= EightPrice; i++)
                {
                    TravelOrderDetail detail = new TravelOrderDetail();
                    detail.Subject = "八人房";
                    detail.Price = (int)travelproduct.EightPrice;
                    travelOrderDetails.Add(detail);
                }


            }
            if (WithoutBedPrice.HasValue && WithoutBedPrice > 0)
            {
                for (int i = 1; i <= WithoutBedPrice; i++)
                {
                    TravelOrderDetail detail = new TravelOrderDetail();
                    detail.Subject = "兒童不佔床";
                    detail.Price = (int)travelproduct.WithoutBedPrice;
                    travelOrderDetails.Add(detail);
                }


            }
            if (BabyPric.HasValue && BabyPric > 0)
            {
                for (int i = 1; i <= BabyPric; i++)
                {
                    TravelOrderDetail detail = new TravelOrderDetail();
                    detail.Subject = "嬰兒(2歲以下)";
                    detail.Price = (int)travelproduct.BabyPric;
                    travelOrderDetails.Add(detail);
                }


            }

            if (OneDayAdult.HasValue && OneDayAdult > 0)
            {
                for (int i = 1; i <= OneDayAdult; i++)
                {
                    TravelOrderDetail detail = new TravelOrderDetail();
                    detail.Subject = "一日遊大人";
                    detail.Price = (int)travelproduct.OneDayAdult;
                    travelOrderDetails.Add(detail);
                }


            }

            if (OneDayChild.HasValue && OneDayChild > 0)
            {
                for (int i = 1; i <= OneDayChild; i++)
                {
                    TravelOrderDetail detail = new TravelOrderDetail();
                    detail.Subject = "一日遊小孩";
                    detail.Price = (int)travelproduct.OneDayChild;
                    travelOrderDetails.Add(detail);
                }


            }

            if (OneDayBaby.HasValue && OneDayBaby > 0)
            {
                for (int i = 1; i <= OneDayBaby; i++)
                {
                    TravelOrderDetail detail = new TravelOrderDetail();
                    detail.Subject = "一日遊嬰兒";
                    detail.Price = (int)travelproduct.OneDayBaby;
                    travelOrderDetails.Add(detail);
                }
            }

            if (travelOrderDetails.Count > 0)
            {
                Session["TravelOrderDetail"] = travelOrderDetails;
                return RedirectToAction("Contract", new { id = id, selectDate = selectDate, Mobile = Mobile });
            }
            return View(travelproduct);
        }
         [Authorize]
        public ActionResult Contract(int Id, DateTime? selectDate)
        {
            TravelProduct travelproduct = _db.TravelProducts.Find(Id);
            if (travelproduct == null)
            {
                return HttpNotFound();
            }

            if (!selectDate.HasValue)
            {
                ViewBag.Message = "請選擇出發日期";
                return View();
            }
            List<TravelOrderDetail> travelOrderDetails = (List<TravelOrderDetail>)Session["TravelOrderDetail"];
            TravelOrder travelorder = (TravelOrder)Session["TravelOrders"] ;
             if (travelorder == null)
             {
                 travelorder=new TravelOrder();
             }

             ViewBag.selectDate = selectDate;
            ViewBag.travelproduct = travelproduct;
            var customers = _db.Customerses.FirstOrDefault(x => x.Account == User.Identity.Name);
            ViewBag.CustomerId = customers.Id;
            ViewBag.SchduleId = Id;
            ViewBag.travelOrderDetails = travelOrderDetails;
             if (Session["Coupon"] != null)
             {
                 ViewBag.Coupon = Session["Coupon"];
             }
             travelorder.OrderName = customers.Name;
             travelorder.OrderAddress = customers.Address;
             travelorder.OrderEmail = customers.Email;
             travelorder.OrderMobile = customers.Mobile;
             travelorder.OrderTelphone = customers.Telphone;


             return View(travelorder);
        }



         [HttpPost]
         public ActionResult Contract(TravelOrder travelorder, List<TravelOrderDetail> travelOrderDetails, String updatecustomer)
         {
             Response.Cache.SetCacheability(HttpCacheability.NoCache);
             //Response.Cache.SetExpires(DateTime.MinValue);
             //Response.Cache.SetNoStore();

             if (ModelState.IsValid)
             {
                 if (updatecustomer == "on")
                 {
                     var customer = _db.Customerses.Find(travelorder.CustomerId);
                     customer.Address = travelorder.OrderAddress;
                     customer.Email = travelorder.OrderEmail;
                     customer.Telphone = travelorder.OrderTelphone;
                     customer.Mobile = String.IsNullOrEmpty(travelorder.OrderMobile) == true ? customer.Mobile : travelorder.OrderMobile;
                     customer.Name = travelorder.OrderName;
                 }

                 foreach (TravelOrderDetail orderDetail in travelOrderDetails)
                 {
                     travelorder.OrderDetailses.Add(orderDetail);
                 }
                 travelorder.OrderStatus = TravelOrderStatus.待處理;
                 travelorder.InitDate = DateTime.Now;

                 _db.TravelOrders.Add(travelorder);
                 _db.SaveChanges();
                 //travelorder.Orderid = "T" + DateTime.Now.ToString("yyyyMMddHHmmss-") + travelorder.Id.ToString("0000000");
                 //_db.SaveChanges();

                 Session["TravelOrderDetail"] = travelOrderDetails;
                 Session["TravelOrders"] = travelorder;

                 if (travelorder.PayType == PayType.自動櫃員機ATM繳費)
                 {
                     //訂單編號 流水號
                     OrderSequence ordersequence = new OrderSequence();
                     _db.OrderSequences.Add(ordersequence);
                     _db.SaveChanges();

                     travelorder.Orderid = "T" + ordersequence.Id.ToString("0000000");

                     Session["TravelOrderDetail"] = null;
                     //Session["TravelOrders"] = null;
                     Session["Coupon"] = null;
                     var coupon = _db.TravelCoupons.FirstOrDefault(x => x.CouponCode == travelorder.Coupon);
                     if (coupon != null)
                     {
                         coupon.IsUse = BooleanType.是;
                     }
                     _db.SaveChanges();

                     //發送Email
                     TravelProduct travelProduct = _db.TravelProducts.Find(travelorder.SchduleId);
                     MailTemplate mailTemplate = new MailTemplate();
                     mailTemplate.MailSubject = "瘋趣台灣-ATM繳費通知";
                     mailTemplate.SendTravelOrderATMEmail(travelorder, travelorder.OrderEmail);

                     return RedirectToAction("GOATM", new {  Mobile =Request["Mobile"]  });
                 }

                 //return RedirectToAction("GoCreditCard");

                 Transaction transaction = new Transaction();
                 transaction.TravelOrderId = travelorder.Id;
                 //transaction.TransactionOrderid = travelorder.Orderid;
                 transaction.Transamt = travelorder.Total;
                 Session["Transaction"] = transaction;
                 return RedirectToAction("GoCreditCard", "Transaction", new { Mobile = Request["Mobile"] });
             }

             ViewBag.selectDate = travelorder.ActityDate;
             TravelProduct travelproduct = _db.TravelProducts.Find(travelorder.SchduleId);
             ViewBag.travelproduct = travelproduct;
             var customers = _db.Customerses.FirstOrDefault(x => x.Account == User.Identity.Name);
             ViewBag.CustomerId = customers.Id;
             ViewBag.travelOrderDetails = travelOrderDetails;
             Session["TravelOrderDetail"] = travelOrderDetails;
             return View(travelorder);
         }

        public ActionResult GoATM()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            TravelOrder travelorder = (TravelOrder)Session["TravelOrders"];
            Session["TravelOrders"] = null;
            return View(travelorder);
        }


      
    }
}
