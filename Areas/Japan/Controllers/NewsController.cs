﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2tw.Controllers;
using Fun2twModels.Models;
using MvcPaging;
using Newtonsoft.Json;

namespace Fun2tw.Areas.Japan.Controllers
{
    public class NewsController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //
        // GET: /News/

        public ActionResult Index(int? page, FormCollection fc)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);

            var newses = _db.Newses.Include(n => n.Catalog).OrderByDescending(p => p.InitDate).AsQueryable();
            ViewBag.CategoryId = new SelectList(_db.NewsCatalogs.OrderBy(p => p.InitDate), "Id", "Subject");
            if (hasViewData("SearchBySubject"))
            {
                string searchBySubject = getViewDateStr("SearchBySubject");
                newses = newses.Where(w => w.Subject.Contains(searchBySubject));
            }

            if (hasViewData("SearchByCategoryId"))
            {
                int searchByCategoryId = getViewDateInt("SearchByCategoryId");
                newses = newses.Where(w => w.CategoryId == searchByCategoryId);
            }

            //            ViewBag.Subject = Subject;
            return View(newses.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));
        }

        //
        // GET: /News/Details/5

        public ActionResult Details(int id = 0)
        {
            News news = _db.Newses.Find(id);
            if (news == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(news);
        }

        [AllowAnonymous]
        public ActionResult GetJson()
        {
            var item = _db.Newses
                .Select(
                c => new
                {
                    Id = c.Id,
                    Subject=c.Subject,
                    InitDate = c.InitDate
                }
                ).OrderByDescending(o => o.InitDate);
            string jsonContent;
            if (item.ToList().Count == 0)
            {
                News news = new News
                {
                    Id = 0,
                    Subject = "目前無資料"
                };
                List<News> list = new List<News> { news };
                jsonContent = JsonConvert.SerializeObject(list, Formatting.Indented);
            }
            else
            {
                jsonContent = JsonConvert.SerializeObject(item.ToList(), Formatting.Indented);
            }
            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }

    }
}
