﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2twModels.Models;

namespace Fun2tw.Areas.Japan.Controllers
{
    public class FAQController : Controller
    {
        private BackendContext db = new BackendContext();
        //
        // GET: /FAQ/

        public ActionResult Index(int? id)
        {
            var faqCategoryes = db.FaqCategories.OrderBy(p => p.ListNum).ToList();
            FAQCategory faqCategory = id != null ? db.FaqCategories.Find(id) : faqCategoryes.FirstOrDefault();
            ViewBag.FaqCategoryes = faqCategoryes;
            return View(faqCategory);
        }

    }
}
