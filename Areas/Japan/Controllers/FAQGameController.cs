﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2tw.Models;
using Fun2twModels.Models;

namespace Fun2tw.Areas.Japan.Controllers
{
    public class FAQGameController : Controller
    {
        private BackendContext _db = new BackendContext();
        //
        // GET: /FAQGame/

        public ActionResult Index()
        {
            string coupon = GetNewCoupon();

            ViewBag.coupon = coupon;
            return View();
        }

        public ActionResult Result()
        {
            string coupon = GetNewCoupon();

            ViewBag.coupon = coupon;
            return View();
        }

        public ActionResult ResultLoop()
        {
            for (int i = 0; i <= 1400; i++)
            {
                string coupon = GetNewCoupon(i);
                System.Threading.Thread.Sleep(8);
            }

            return Content("success");
        }

        private string GetNewCoupon()
        {
            string coupon = "";
            bool isExist;
            var coupons = _db.TravelCoupons.Select(
                 c => new
                 {
                     CouponCode = c.CouponCode
                 }).ToList();
            do
            {
                coupon = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 6);
                var c = new { CouponCode = coupon };
                isExist = coupons.Contains(c);

            } while (isExist);
            TravelCoupon NewCoupon = new TravelCoupon();
            NewCoupon.CouponCode = coupon;
            NewCoupon.IsUse = BooleanType.否;
            NewCoupon.InitDate = DateTime.Now;
            NewCoupon.IPAddress = Utility.GetIPAddress();
            NewCoupon.EndDate =Convert.ToDateTime( "2017/1/1");
            _db.TravelCoupons.Add(NewCoupon);
            _db.SaveChanges();
            return coupon;
        }

        private string GetNewCoupon(int i)
        {
            string[] IPs = { "222.141.22.10", "140.22.44", "210.191.242.5", "210.200.141.91", "124.108.105.20", "222.124.141.92", "163.29.121.8", "210.200.141.3", "140.127.1.150", "140.127.1.22", "140.127.5.110", "124.108.105.150", "163.29.121.5", "210.69.82.243", "104.217.226.242" };

            DateTime d = DateTime.Now.AddDays(-11);
            string coupon = "";
            bool isExist;
            var coupons = _db.TravelCoupons.Select(
                 c => new
                 {
                     CouponCode = c.CouponCode
                 }).ToList();
            do
            {
                coupon = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 6);
                var c = new { CouponCode = coupon };
                isExist = coupons.Contains(c);

            } while (isExist);
            TravelCoupon NewCoupon = new TravelCoupon();
            NewCoupon.CouponCode = coupon;
            NewCoupon.IsUse = BooleanType.否;
            NewCoupon.InitDate = d.AddMinutes(i * -30).AddSeconds(i*-33);
            int IpIndex = i%14;
            NewCoupon.IPAddress = IPs[IpIndex];
            _db.TravelCoupons.Add(NewCoupon);
            _db.SaveChanges();
            return coupon;
        }
    }
}
