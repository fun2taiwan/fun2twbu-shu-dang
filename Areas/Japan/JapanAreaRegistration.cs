﻿using System.Web.Mvc;

namespace Fun2tw.Areas.Japan
{
    public class JapanAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Japan";
            }
        }



        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Japan_default",
                "Japan/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
