﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Device.Location;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2tw.Controllers;
using Fun2tw.Models;
using Fun2twModels.Models;
using MvcPaging;
using Newtonsoft.Json;

namespace Fun2tw.Areas.English.Controllers
{
    public class ShopInfoController : _BaseController
    {
        //
        // GET: /ShopInfo/

        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 16;
        //




        public ActionResult Index(int? page, FormCollection fc, string[] Category = null, string[] Town = null, int id = 0)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);


            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);

            var shopinfos = _db.ShopInfos.AsQueryable();

            //if (hasViewData("SearchBySubject"))
            //{
            //    string searchBySubject = getViewDateStr("SearchBySubject");
            //    shopinfos = shopinfos.Where(w => w.Subject.Contains(searchBySubject));
            //}
            string[] Towns = new string[] { };
            if (Town != null)
            {

                Towns = Town;
            }
            else if (Session["ShopInfoTown"] != null)
            {
                Towns = (string[])Session["ShopInfoTown"];
            }
            string[] Categorys = new string[] { };
            if (Category != null)
            {
                Categorys = Category;
            }
            else if (Session["ShopInfoCategory"] != null)
            {
                Categorys = (string[])Session["ShopInfoCategory"];
            }

            if (Towns.Length>0)
            {
                shopinfos = shopinfos.Where(x => x.Id == 0);
                foreach (string item in Towns)
                {
                    //Linq或的語法
                    shopinfos = shopinfos.Concat(_db.ShopInfos.Where(x => x.Category.Contains(item)));
                }
            }
            var shopinfos1 = _db.ShopInfos.OrderByDescending(p => p.InitDate).AsQueryable();
            if (Categorys.Length>0)
            {
                shopinfos1 = shopinfos1.Where(x => x.Id == 0);
                foreach (string item in Categorys)
                {
                    //Linq或的語法
                    shopinfos1 = shopinfos1.Concat(_db.ShopInfos.Where(x => x.Category.Contains(item)));
                }
            }
            if (shopinfos1.Any())
            {
                shopinfos = shopinfos.Intersect(shopinfos1);
            }

            shopinfos = shopinfos.Distinct();


            ViewBag.ClassId = _db.ShopInfoCategories.OrderBy(x => x.ListNum).ToList();
            ViewBag.selectClassId = Categorys;
            ViewBag.selectTown = Towns;
            Session["ShopInfoCategory"] = Categorys;
            Session["ShopInfoTown"] = Towns;
            //            ViewBag.Subject = Subject;
            return View(shopinfos.OrderByDescending(p => p.ListNum).ThenByDescending(x => x.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));
        }

        [HttpPost]
        public ActionResult Index(int? page, FormCollection fc, string[] Category = null, string[] Town = null)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);


            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);

            var shopinfos = _db.ShopInfos.OrderByDescending(p => p.InitDate).AsQueryable();

            //if (hasViewData("SearchBySubject"))
            //{
            //    string searchBySubject = getViewDateStr("SearchBySubject");
            //    shopinfos = shopinfos.Where(w => w.Subject.Contains(searchBySubject));
            //}
            string[] Towns = new string[] { };
            if (Town != null)
            {
                Towns = Town;
            }
           
            string[] Categorys = new string[] { };
            if (Category != null)
            {
                Categorys = Category;
            }
          

            if (Towns.Length > 0)
            {
                shopinfos = shopinfos.Where(x => x.Id == 0);
                foreach (string item in Towns)
                {
                    //Linq或的語法
                    shopinfos = shopinfos.Concat(_db.ShopInfos.Where(x => x.Category.Contains(item)));
                }
            }
            var shopinfos1 = _db.ShopInfos.AsQueryable();
            if (Categorys.Length > 0)
            {
                shopinfos1 = shopinfos1.Where(x => x.Id == 0);
                foreach (string item in Categorys)
                {
                    //Linq或的語法
                    shopinfos1 = shopinfos1.Concat(_db.ShopInfos.Where(x => x.Category.Contains(item)));
                }
            }
            if (shopinfos1.Any())
            {
                shopinfos = shopinfos.Intersect(shopinfos1);
            }

            shopinfos = shopinfos.Distinct();


            ViewBag.ClassId = _db.ShopInfoCategories.OrderBy(x => x.ListNum).ToList();
            ViewBag.selectClassId = Categorys;
            ViewBag.selectTown = Towns;
            Session["ShopInfoCategory"] = Categorys;
            Session["ShopInfoTown"] = Towns;
            //            ViewBag.Subject = Subject;
            return RedirectToAction("Index");
        }


        public ActionResult OverView(FormCollection fc)
        {


            var shopInfoCategories = _db.ShopInfoCategories.OrderBy(x => x.ListNum).ToList();

            var shopinfos1 = _db.ShopInfos.Where(x => x.Category.Contains("星級飯店") || x.Category.Contains("山莊農場") || x.Category.Contains("在地民宿") || x.Category.Contains("優質旅店") || x.Category.Contains("露營地區")).OrderByDescending(p => p.InitDate).Take(4).ToList();

            var shopinfos2 = _db.ShopInfos.Where(x => x.Category.Contains("水上活動") || x.Category.Contains("陸上活動") || x.Category.Contains("夜間活動") || x.Category.Contains("山野風情") || x.Category.Contains("遊樂園區")).OrderByDescending(p => p.InitDate).Take(4).ToList();

            var shopinfos3 = _db.ShopInfos.Where(x => x.Category.Contains("在地美食") || x.Category.Contains("異國風味") || x.Category.Contains("西式餐點") || x.Category.Contains("海鮮料理")).OrderByDescending(p => p.InitDate).Take(4).ToList();

            var shopinfos4 = _db.ShopInfos.Where(x => x.Category.Contains("農產品") || x.Category.Contains("伴手禮") || x.Category.Contains("紀念品")).OrderByDescending(p => p.InitDate).Take(4).ToList();

            ViewBag.shopinfos1 = shopinfos1;
            ViewBag.shopinfos2 = shopinfos2;
            ViewBag.shopinfos3 = shopinfos3;
            ViewBag.shopinfos4 = shopinfos4;

            return View(shopInfoCategories);
        }

        //
        // GET: /ShopInfo/Details/5

        public ActionResult Details(int id = 0)
        {
            ShopInfo shopinfo = _db.ShopInfos.Find(id);
            if (shopinfo == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(shopinfo);
        }

        public ActionResult GetJson()
        {

            string jsonContent;
            var shopinfo = _db.ShopInfos.Where(x => x.Enable == BooleanType.是).Select(
                  c => new
                  {
                      Id = c.Id,
                      Subject = c.Subject,
                      UpImageUrl = c.Pictures.Count > 0 ? "https://www.fun2tw.com/upfiles/images/" + c.Pictures.FirstOrDefault().UpImageUrl : "",
                      Tel=c.Tel ?? "",
                      InitDate = c.InitDate,
                      DisShow="請開啟GPS",
                      Lat = c.Lat,
                      Lon = c.Lon,
                      Type = "Shop",
                        Address = c.Address,
                        
                  });
            jsonContent = JsonConvert.SerializeObject(shopinfo, Formatting.Indented);



            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }

        public ActionResult GetJsonbyLocation(string lon, string lat)
        {
            //int dis = 100000;             //以公尺為單位
            double Lon = 0;
            double Lat = 0;

            if (double.TryParse(lon, out Lon))
            {
                Lon = double.Parse(lon);
                if (Lon < -180 || Lon > 180)
                {
                    return Content("參數lon：位置的經度。 這個值範圍可以從 -180.0 到 180.0。");
                }
            }
            if (double.TryParse(lat, out Lat))
            {
                Lat = double.Parse(lat);
                if (Lat < -90 || Lat > 90)
                {
                    return Content("參數lat：位置的緯度。 這個值範圍可以從 -90.0 到 90.0。");
                }
            }

            if (Lon == 0 && Lat == 0)
            {
                return HttpNotFound();
            }


            var refuges = _db.ShopInfos.ToList();
            var coord = new GeoCoordinate(Lat, Lon);
            var jsonitem = refuges.Where(x => !string.IsNullOrEmpty(x.Lat) && !string.IsNullOrEmpty(x.Lon)
                && double.Parse(x.Lat) <= 90 && double.Parse(x.Lat) >= -90
                && double.Parse(x.Lon) <= 180 && double.Parse(x.Lon) >= -180).Select(
                    c => new MapItem
                    {
                        Id = c.Id.ToString(),
                        Subject = c.Subject,
                        UpImageUrl = c.Pictures.Count > 0 ? "https://www.fun2tw.com/upfiles/images/" + c.Pictures.FirstOrDefault().UpImageUrl : "",
                        Lat = c.Lat,
                        Lon = c.Lon,
                        Dis = (int)(new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord),
                        DisShow = (new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord) > 1000 ? string.Format("{0:0.0}公里", (new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord) / 1000) : "附近",
                        Type = "Shop",
                        Address = c.Address,
                        Tel = c.Tel ?? ""
                    }
                )
                .OrderBy(o => o.Dis)
                .ToList();

            //List<ScheduleModel.Models.Health> item2 = db2.Healths.ToList();
            //var jsonitem2 = item2.Where(x => !string.IsNullOrEmpty(x.Lat) && !string.IsNullOrEmpty(x.Lon)
            //    && double.Parse(x.Lat) <= 90 && double.Parse(x.Lat) >= -90
            //    && double.Parse(x.Lon) <= 180 && double.Parse(x.Lon) >= -180
            //    && (new GeoCoordinate(double.Parse(x.Lat), double.Parse(x.Lon))).GetDistanceTo(coord) <= dis).Select(
            //         c => new MapItem
            //         {
            //             Id = c.Id.ToString(),
            //             Subject = c.Subject,
            //             Lat = c.Lat,
            //             Lon = c.Lon,
            //             Dis = (int)((new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord)),
            //             DisShow = (new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord) > 1000 ? string.Format("{0:0.0}公里", (new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord) / 1000) : string.Format("{0}公尺", (int)(new GeoCoordinate(double.Parse(c.Lat), double.Parse(c.Lon))).GetDistanceTo(coord)),
            //             Type = GetCategoryName(c.Category),
            //             Address = c.Address
            //         }
            //    )
            //    .OrderBy(o => o.Dis)
            //    .ToList();

            var jsonitem3 = jsonitem.OrderBy(o => o.Dis);

            string jsonContent = JsonConvert.SerializeObject(jsonitem3, Formatting.Indented);
            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }

    }
}
