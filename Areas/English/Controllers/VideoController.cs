﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2twModels.Models;
using MvcPaging;

namespace Fun2tw.Areas.English.Controllers
{
    public class VideoController : Controller
    {
        private BackendContext _db = new BackendContext();

        public ActionResult Index(int? page, FormCollection fc)
        {
            var videos = _db.ThemeVideos.Take(5).OrderByDescending(p => p.InitDate);
            var topVideo = videos.FirstOrDefault();
            ViewBag.topVideo = topVideo;
            return View(videos.Skip(1).Take(4).ToList());
        }


    }
}
