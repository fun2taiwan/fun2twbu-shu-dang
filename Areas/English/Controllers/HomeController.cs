﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2tw.Models;
using Fun2twModels.Models;
using Newtonsoft.Json;

namespace Fun2tw.Areas.English.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        private BackendContext db = new BackendContext();

        public ActionResult Index()
        {
            var travelproducts =
                db.TravelProducts.Where(x => x.Enable == BooleanType.是 && x.UpImageUrl != null).AsQueryable();
            ViewBag.travelproducts = travelproducts;

            var themeVideos = db.ThemeVideos.AsQueryable();
            ViewBag.ThemeVideos = themeVideos;

            DateTime yesday = DateTime.Now.AddDays(-1);
            var News =db.Newses.Where(x => x.StartDate < DateTime.Now && x.EndDate > yesday).AsQueryable();
            ViewBag.News = News;

            var banners = db.HomeBanners.Where(x => x.Status == BooleanType.是).AsQueryable();
            ViewBag.HomeBanners = banners;

            var aboutLinks = db.AboutLinks.Where(x => x.Status == BooleanType.是).AsQueryable();
            ViewBag.AboutLinks = aboutLinks;

            var expertPlans = db.ExpertPlans.Where(x=>x.UpImageUrl!=null).AsQueryable();
            ViewBag.expertPlans = expertPlans;
            return View();
        }

        public ActionResult PreRegister()
        {

            return View();
        }

        public ActionResult GmailRegister()
        {

            return View();
        }

        [HttpPost]
        public ActionResult GmailRegister(string id,string Email, string Name, string ImageUrl,string Poster,string Source)
        {
            var exCustomers = db.Customerses.Where(x => x.Account == id);
            if (exCustomers.Any())
            {
                string userData = JsonConvert.SerializeObject(exCustomers.FirstOrDefault());
                
                Utility.SetAuthenTicket(userData, exCustomers.FirstOrDefault().Account);
                return Content(id + "這個帳號已使用!");
            }
            try
            {
                Customers customers = new Customers
                            {
                                Account = id,
                                Email = Email,
                                ImageUrl = ImageUrl,
                                Name = Name,
                                Gender = GenderType.不指定,
                                Enable = BooleanType.是,
                                InitDate = DateTime.Now,
                                PasswordSalt = Utility.CreateSalt(),
                                Poster = Poster,
                                Source = Source
                            };
                //customers.Password = "         "　;
                db.Customerses.Add(customers);
                db.SaveChanges();

                string userData = JsonConvert.SerializeObject(customers);
                Utility.SetAuthenTicket(userData, customers.Account);

                //customers.Create(db, db.Customerses);
                return Content("Success");
            }
            catch (Exception e)
            {

                return Content(e.Message);
            }
        }
        //
        // POST: /Customer/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PreRegister(PreRegisterView preRegister)
        {
            if (ModelState.IsValid)
            {
                var exCustomers = db.Customerses.Where(x => x.Account == preRegister.Account);
                if (exCustomers.Any())
                {
                    ViewBag.Message = "帳號重複!";
                    return View(preRegister);
                }
                if (preRegister.Account.IndexOf('@') <= 0 && preRegister.Account.IndexOf('0') != 0)
                {
                    ViewBag.Message = "帳號格式錯誤!須為手機或E-mail";
                    return View(preRegister);
                }

                Customers customers = preRegister.GetCustomers();

                db.Customerses.Add(customers);
                customers.Create(db, db.Customerses);

                return RedirectToAction("Success");
            }

            return View(preRegister);
        }

        public ActionResult Success()
        {

            return View();
        }

        public ActionResult Helper()
        {

            return View();
        }

        public ActionResult OpenData()
        {
            var themeVideos = db.ThemeVideos.OrderBy(x => x.InitDate).ToList();

            return View(themeVideos);
        }

        public ActionResult Policy()
        {

            return View();
        }

        public ActionResult Ticket()
        {
            var travelproducts =
               db.TravelProducts.Where(x => x.Enable == BooleanType.是 && x.UpImageUrl != null).OrderByDescending(p => p.InitDate).Take(4).ToList();
            ViewBag.travelproducts = travelproducts;
            return View();
        }
    }
}
