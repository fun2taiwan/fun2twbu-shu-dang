﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2tw.Models;
using Fun2twModels.Models;

namespace Fun2tw.Areas.English.Controllers
{
    public class NavigationController : Controller
    {
        private BackendContext _db = new BackendContext();

        //
        // GET: /Navigation/

        public ActionResult Index()
        {
            return View(_db.Navigations.ToList());
        }



        public ActionResult AddLocation(string Subject, string Address, string Lon, string Lat)
        {

            List<Navigation> addressList;

            String result;
            if (Session["addressList"] == null)
            {
                addressList = new List<Navigation>();
            }
            else
            {
                addressList = (List<Navigation>)Session["addressList"];
            }

            //重複加入
            if (addressList.Any(x=>x.Subject==Subject))
            {
                result = "duplicate";
            }
            else if (addressList.Count >= 10)
            {
                result = "over";
            }
            else
            {
                var navigation = new Navigation
                {
                    Subject = Subject,
                    Address = Address,
                    Lat = Lat,
                    Lon = Lon
                };


                addressList.Add(navigation);
                result = addressList.Count.ToString();
            }

            Session["addressList"] = addressList;
            return Content(result);
        }




        //public ActionResult SortLocation(int[] location)
        //{
        //    if (location == null)
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    if (location.Length == 1)
        //    {
        //        ViewBag.Message = "請選擇兩個景點以上";
        //    }
        //    if (location.Length > 10)
        //    {
        //        ViewBag.Message = "Google MAP 僅支援10個景點內路線規劃";

        //    }
        //    var navigation = _db.Navigations.Where(x => location.Contains(x.Id)).ToList();
        //    return View(navigation.ToList());
        //}

        public ActionResult SortLocation()
        {
            List<Navigation> addressList;

            if (Session["addressList"] == null)
            {
                addressList = new List<Navigation>();
            }
            else
            {
                addressList = (List<Navigation>)Session["addressList"];
            }


            if (addressList.Count <= 1)
            {
                ViewBag.Message = "請選擇兩個以上的景點";
            }
            if (addressList.Count > 10)
            {
                ViewBag.Message = "Google MAP 僅支援10個景點內路線規劃";
            }




            return View(addressList);
        }


        //[HttpPost]
        //public ActionResult Result(string[] address)
        //{

        //    //Some code..........
        //    List<string> addressList = new List<string>();
        //    foreach (string location in address)
        //    {
        //        addressList.Add(location);
        //    }
        //    ViewBag.Address = addressList;

        //    return View();
        //    //return View(addressList);
        //}

        public ActionResult Result(List<TravelNavigation> travel,string isSave="")
        {
            if (travel == null)
            {
                return RedirectToAction("SortLocation");
            }
            List<int> addressList=new List<int>();
            foreach (var item in travel.OrderBy(x=>x.listNum))
            {
                addressList.Add(item.id);
            }
            //Session["addressList"] = addressList;

            ViewBag.isSave = isSave;
            return View(travel);
        }


        //
        // GET: /Navigation/Delete/5

        public ActionResult Remove(int id = 0)
        {
            List<int> addressList;
            if (Session["addressList"] == null)
            {
                addressList = new List<int>();
            }
            else
            {
                addressList = (List<int>)Session["addressList"];
            }

            addressList.Remove(id);

            Session["addressList"] = addressList;

            return RedirectToAction("SortLocation");
        }

        public ActionResult OverView()
        {

            return View();
        }

        public ActionResult Plan()
        {

            return View();
        }

        [Authorize]
        public ActionResult Save()
        {
            List<int> addressLists;
            if (Session["addressList"] == null)
            {
                return RedirectToAction("Plan");
            }

            addressLists = (List<int>)Session["addressList"];

            string DetailsIDs = "";
            foreach (int item in addressLists)
            {
                DetailsIDs += item.ToString() + " ";
            }
            CustomerLocation location = new CustomerLocation();
            location.DetailsIDs = DetailsIDs.Trim();
            Customers customers = _db.Customerses.FirstOrDefault(x => x.Account == User.Identity.Name);
            location.CustomerId = customers.Id;

            int[] locationarray = addressLists.ToArray();
            var travelplans = _db.TravelPlans.Where(x => locationarray.Contains(x.Id)).ToList();

            List<TravelPlan> paList=new List<TravelPlan>();
            foreach (int item in addressLists)
            {

                var newitem = travelplans.FirstOrDefault(x => x.Id == item);
                paList.Add(newitem);
            }


            ViewBag.travelplans = paList;

            return View(location);



        }

        [Authorize]
        [HttpPost]
        public ActionResult Save(CustomerLocation location)
        {
            location.InitDate = DateTime.Now;
            _db.CustomerLocations.Add(location);
            _db.SaveChanges();

            return RedirectToAction("MyLocaltion");
        }

        [Authorize]
        public ActionResult MyLocaltion()
        {
            Customers customers = _db.Customerses.FirstOrDefault(x => x.Account == User.Identity.Name);


            return View(customers.CustomerLocations.OrderByDescending(x=>x.InitDate).ToList());
        }

        [Authorize]
        public ActionResult MyLocaltionDetails(int id = 0)
        {
            List<int> addressList;
            CustomerLocation customerLocation = _db.CustomerLocations.Find(id);
            if (customerLocation == null)
            {
                return RedirectToAction("MyLocaltion");
            }


            //int[] location = Array.ConvertAll(addressList.ToArray(), s => int.Parse(s)); 
            string[] DetailsIDs = customerLocation.DetailsIDs.Split(' ');
            List<int> addressLists = new List<int>();
            foreach (string detailsID in DetailsIDs)
            {
                addressLists.Add(Convert.ToInt16(detailsID) );
            }
            int[] locationarray = addressLists.ToArray();
            var travelplans = _db.TravelPlans.Where(x => locationarray.Contains(x.Id)).ToList();
            List<TravelPlan> paList = new List<TravelPlan>();
            foreach (int item in addressLists)
            {

                var newitem = travelplans.FirstOrDefault(x => x.Id == item);
                paList.Add(newitem);
            }
            return View(paList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteLocation(int id)
        {
            CustomerLocation location = _db.CustomerLocations.Find(id);
            _db.CustomerLocations.Remove(location);
            _db.SaveChanges();
            return RedirectToAction("MyLocaltion");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}