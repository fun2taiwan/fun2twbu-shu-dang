﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2tw.Models;
using Fun2twModels.Models;
using HppApi;
using System.Configuration;
using TravelOrder = Fun2twModels.Models.TravelOrder;


namespace Fun2tw.Areas.English.Controllers
{
    public class TransactionController : Controller
    {
        private readonly BackendContext _db = new BackendContext();

        //public static string GenerateOrderId(string typeCode, int transactionId)
        //{
        //    string orderid = typeCode + DateTime.Now.ToString("yyyyMMddHHmmss-") + transactionId.ToString("0000000");

        //    return orderid;
        //}

        [Authorize]
        public ActionResult GoCreditCard()
        {

            if (Session["Transaction"] == null)
            {
                //return HttpNotFound();
                return RedirectToAction("Index", "Home");
            }

            Transaction transaction = (Transaction)Session["Transaction"];
            transaction.Responsemsg = "";

            var travelorder = _db.TravelOrders.Find(transaction.TravelOrderId);

            //訂單編號 流水號
            OrderSequence ordersequence = new OrderSequence();
            _db.OrderSequences.Add(ordersequence);
            _db.SaveChanges();

            travelorder.Orderid = "T" + ordersequence.Id.ToString("0000000");
            _db.SaveChanges();

            transaction.TransactionOrderid = travelorder.Orderid;


            //String key;
            String DomainName = System.Web.Configuration.WebConfigurationManager.AppSettings["DomainName"];
            String RequestURL = System.Web.Configuration.WebConfigurationManager.AppSettings["RequestURL"];

            String transMode = "0"; //交易種類 一般交易時，其值為 0• 分期交易時，其值為 1• 紅利折抵交易時，其值為 2
            String installment = "0"; //分期期數
            String transAmt = transaction.Transamt.ToString(CultureInfo.InvariantCulture); //金額
            String template = System.Web.Configuration.WebConfigurationManager.AppSettings["Template"];
            String notifyURL = System.Web.Configuration.WebConfigurationManager.AppSettings["NotifyURL"]; //回應網址
            //String cssURL = System.Web.Configuration.WebConfigurationManager.AppSettings["CssURL"]; //CSS網址

            //String orderType = "T";


            transaction.Merchantid = System.Web.Configuration.WebConfigurationManager.AppSettings["MerchantId"];
            transaction.Terminalid = System.Web.Configuration.WebConfigurationManager.AppSettings["TerminalId"];


            //_db.Transactions.Add(transaction);
            //_db.SaveChanges();

            //transaction.TransactionOrderid = GenerateOrderId(orderType, transaction.Id);
            //_db.SaveChanges();

            ApiClient apiClient = new ApiClient();
            apiClient.setORDERID(transaction.TransactionOrderid);
            apiClient.setMERCHANTID(transaction.Merchantid);
            apiClient.setTERMINALID(transaction.Terminalid);

            apiClient.setTRANSMODE(transMode);  //交易種類 一般交易時，其值為 0• 分期交易時，其值為 1• 紅利折抵交易時，其值為 2
            apiClient.setTemplate(template);
            apiClient.setINSTALLMENT(installment);  //分期期數

            apiClient.setTRANSAMT(transAmt); //金額
            apiClient.setNotifyURL(notifyURL);
            apiClient.setURL(DomainName, RequestURL);
            //apiClient.setCSS_URL(cssURL);

            //try
            //{
            //_db.Transactions.Add(transaction);
            //_db.SaveChanges();

            //transaction.Orderid = TransactionOrder.GenerateOrderId("ptbus-ts", transaction.Id);
            //_db.SaveChanges();
            //}
            //catch (DbEntityValidationException ex)
            //{
            //    var entityError = ex.EntityValidationErrors.SelectMany(x => x.ValidationErrors).Select(x => x.ErrorMessage);
            //    var getFullMessage = string.Join("; ", entityError);
            //    var exceptionMessage = string.Concat(ex.Message, "errors are: ", getFullMessage);
            //    //NLog
            //    //LogException(new Exception(string.Format("File : {0} {1}.", logFile.FullName, exceptionMessage), ex));
            //    ViewBag.Message = exceptionMessage;
            //    //transaction.Responsemsg = exceptionMessage;
            //}

            //apiClient.setORDERID(transaction.Orderid);


            //產生金鑰動作
            transaction.Returncode = apiClient.postTransaction();
            transaction.Responsecode = apiClient.getRESPONSECODE();
            transaction.Responsemsg = apiClient.getRESPONSEMSG();
            transaction.Key = apiClient.getKEY(); //交易金鑰

            //測試用
            //transaction.Key = "";
            //transaction.Responsecode = "TT";


            if ("00".Equals(transaction.Responsecode))
            {
                //key = apiClient.getKEY(); //交易金鑰
                ViewBag.Key = transaction.Key;
                ViewBag.Orderid = transaction.TransactionOrderid;
                ViewBag.Message = transaction.Responsemsg;
            }
            //else if ("932".Equals(transaction.Responsecode))  //訂單編號重複(之前刷失敗重刷)
            //{
            //    //key = apiClient.getKEY(); //交易金鑰
            //    ViewBag.Key = key;
            //    ViewBag.Orderid = transaction.TransactionOrderid;
            //    ViewBag.Message = transaction.Responsemsg;
            //}
            else
            {
                //_db.Transactions.Add(transaction);
                //_db.SaveChanges();

                Session["Transaction"] = transaction;
                //return RedirectToAction("Contract", new RouteValueDictionary(new { controller = "TravelSchdule", action = "Contract", Id = travelorder.SchduleId }));
                return RedirectToAction("TransactionError");
            }

            //return View();
            String url = "https://" + System.Web.Configuration.WebConfigurationManager.AppSettings["DomainName"] + System.Web.Configuration.WebConfigurationManager.AppSettings["RequestURL"] + "?KEY=" + apiClient.UrlEncode(transaction.Key);
            return Redirect(url);
        }

        //public ActionResult TransactionInitialError()
        //{
        //    Transaction transaction = (Transaction)Session["Transaction"];
        //    return View(transaction);
        //}

        public ActionResult TransactionError()
        {
            Transaction transaction = (Transaction)Session["Transaction"];
            TravelOrder travelorder = (TravelOrder)Session["TravelOrders"];
            ViewBag.travelorder = travelorder;

            if (String.IsNullOrEmpty(transaction.Responsemsg))
            {
                TransactionMessage transMsg = new TransactionMessage();
                ViewBag.Message = transMsg.GetMessage(transaction.Responsecode);
            }
            else
            {
                ViewBag.Message = transaction.Responsemsg;
            }
            return View(transaction);
        }

        [Authorize]
        public ActionResult TransactionResponse()
        {

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetExpires(DateTime.MinValue);
            //Response.Cache.SetNoStore();

            Boolean isSuccess = false;

            String DomainName = System.Web.Configuration.WebConfigurationManager.AppSettings["DomainName"];
            String RequestURL = System.Web.Configuration.WebConfigurationManager.AppSettings["RequestURL"];

            TravelOrder travelOrder = null;
           
            ApiClient apiClient = new ApiClient();

            apiClient.setURL(DomainName, RequestURL);
            apiClient.setKEY(Request.QueryString["KEY"]);
            //apiClient.setKEY("/e7eFc3R3D77YB2mbUYdqjxMqdu8GIqMjR0hb9Ik/oo7ezV38dCzoA==");
         
            int ReturnCode = apiClient.postQuery();

            ////訂單已經存在刷卡成功
            //if (ReturnCode > 0)
            //{
            //    var payedTrans = _db.Transactions.FirstOrDefault(f => f.TransactionOrderid == apiClient.getORDERID() && f.Approvecode.Length > 0 && f.Transcode == Transcode.授權);
            //    if (payedTrans != null)
            //    {
            //        RedirectToAction("Index", "Home");
            //    }
            //}

            Transaction transaction = new Transaction();
            transaction.Key = apiClient.getKEY();
            if (ReturnCode > 0)
            {
                String Orderid = apiClient.getORDERID();

                //transaction = _db.Transactions.FirstOrDefault(t => t.TransactionOrderid == Orderid);
                travelOrder = _db.TravelOrders.FirstOrDefault(t => t.Orderid == Orderid);

                transaction.TravelOrderId = travelOrder.Id;
                transaction.Returncode = ReturnCode;
                transaction.TransactionOrderid = Orderid;
                transaction.Merchantid = apiClient.getMERCHANTID();
                transaction.Terminalid = apiClient.getTERMINALID();
                transaction.Approvecode = apiClient.getAPPROVECODE();
                transaction.Responsecode = apiClient.getRESPONSECODE();
                transaction.Pan = apiClient.getPAN();

                switch (apiClient.getTRANSCODE())
                {
                    case "00":
                        transaction.Transcode = Transcode.授權;
                        break;
                    case "01":
                        transaction.Transcode = Transcode.取消;
                        break;
                }


                switch (apiClient.getTRANSMODE())
                {
                    case "0":
                        transaction.Transmode = Transmode.一般;
                        break;
                    case "1":
                        transaction.Transmode = Transmode.分期;
                        break;
                    case "2":
                        transaction.Transmode = Transmode.紅利;
                        break;
                }

                transaction.Transamt = Convert.ToSingle(apiClient.getTRANSAMT());
                transaction.Transdate = apiClient.getTRANSDATE();
                transaction.Transtime = apiClient.getTRANSTIME();
                transaction.Installment = apiClient.getINSTALLMENT();

                switch (apiClient.getINSTALLMENTTYPE())
                {
                    case "E":
                        transaction.InstallmentType = InstallmentType.外加;
                        break;
                    case "I":
                        transaction.InstallmentType = InstallmentType.內合;
                        break;
                }

                //首期金額
                transaction.Firstamt = apiClient.getFIRSTAMT();
                //每期金額
                transaction.Eachamt = apiClient.getEACHAMT();
                //分期手續費 
                transaction.Fee = apiClient.getFEE();

                //紅利折抵方式
                switch (apiClient.getREDEEMTYPE()) // 1 –全額，2 –部份 
                {
                    case "1":
                        transaction.Redeemtype = Redeemtype.全額;
                        break;
                    case "2":
                        transaction.Redeemtype = Redeemtype.部份;
                        break;
                }

                //紅利折抵點數 
                transaction.Redeemused = apiClient.getREDEEMUSED();
                //紅利餘額 
                transaction.Redeembalance = apiClient.getREDEEMBALANCE();
                //持卡人自付金額
                transaction.Creditamt = apiClient.getCREDITAMT();
                //風險卡號註記 
                transaction.RiskMark = apiClient.getRiskMark(); // Y:表示風險卡號

                //國外卡 
                switch (apiClient.getForeign()) //若為Visa、MasterCard及JCB卡時： Y－國外卡 N－國內卡 否則為空白
                {
                    case "Y":
                        transaction.Foreign = Foreign.國外卡;
                        break;
                    case "N":
                        transaction.Foreign = Foreign.國內卡;
                        break;
                }

                //3D認證結果
                switch (apiClient.getSecureStatus()) //若特店及端末機啟用3D功能時： Y－成功 N－失敗 B－未執行3D認證否則為空白（未啟用3D功能）
                {
                    case "Y":
                        transaction.SecureStatus = SecureStatus.成功;
                        break;
                    case "N":
                        transaction.SecureStatus = SecureStatus.失敗;
                        break;
                    case "B":
                        transaction.SecureStatus = SecureStatus.未執行3D認證;
                        break;
                }

                //已經付款成功的  防止按上一步 又加入一筆新的transaction
                if (travelOrder != null && travelOrder.OrderStatus == TravelOrderStatus.已付款)
                {
                    return View(transaction);
                }

                //_db.SaveChanges();
                if (transaction.Transcode == Transcode.授權 && (transaction.Responsecode.Equals("00") || transaction.Responsecode.Equals("08") || transaction.Responsecode.Equals("11")) && transaction.Approvecode.Length > 0)
                {
                    ViewBag.Message = "信用卡付款成功";
                    travelOrder.OrderStatus = TravelOrderStatus.已付款;
                    isSuccess = true;

                    //付款成功 coupon標記已使用
                    Session["TravelOrder"] = null;
                    Session["TravelOrderDetail"] = null;
                    Session["Coupon"] = null;
                    var coupon = _db.TravelCoupons.FirstOrDefault(x => x.CouponCode == travelOrder.Coupon);
                    if (coupon != null)
                    {
                        coupon.IsUse = BooleanType.是;
                    }
                }
                else
                {
                    ViewBag.Message = "信用卡付款失敗";
                    Session["Transaction"] = transaction;
                    isSuccess = false;
                    //return RedirectToAction("TransactionError");
                }

            }
            else
            {
                ViewBag.Message = "發生錯誤";
                Session["Transaction"] = transaction;
                isSuccess = false;
                //return RedirectToAction("TransactionError");
            }

         

            //儲存變更
            if (ModelState.IsValid)
            {
                _db.Transactions.Add(transaction);
                try
                {
                    _db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
            }

            if (isSuccess == false)
            {
                return RedirectToAction("TransactionError");
            }
            else
            {
                //刷卡成功 發送Email
                TravelProduct travelProduct = _db.TravelProducts.Find(travelOrder.SchduleId);
                List<TravelOrderDetail> travelOrderDetails = travelOrder.OrderDetailses.ToList();
                //var customer = _db.Customerses.Find(travelOrder.CustomerId);
                MailTemplate mailTemplate = new MailTemplate();
                mailTemplate.MailSubject = "瘋趣台灣-刷卡成功通知，訂單編號:" + travelOrder.Orderid.ToString();
                mailTemplate.SendTravelOrderEmail(travelOrder, travelProduct, travelOrderDetails, travelOrder.OrderEmail);
                //刷卡成功  session 清除
                Session["Transaction"] = null;
            }

            return View(transaction);
        }



        public ActionResult Test()
        {            
            return View();
        }

        public ActionResult Test2()
        {
            //OrderSequence os = new OrderSequence();
            //_db.OrderSequences.Add(os);
            //_db.SaveChanges();
            //ViewBag.OrderId = os.Id;
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}