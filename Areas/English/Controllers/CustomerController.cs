﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2tw.Models;
using Fun2twModels.Models;
using Google;
using Newtonsoft.Json;
using System.Configuration;

namespace Fun2tw.Areas.English.Controllers
{
    public class CustomerController : Controller
    {
        private BackendContext _db = new BackendContext();
        //
        // GET: /Customer/
        public ActionResult Login(string account)
        {
            var customers = _db.Customerses.FirstOrDefault(x => x.Account == account);
            if (customers == null)
            {
                Customers newCustomers = new Customers
                {
                    Account = account,
                    Email = account,
                    Name = account,
                    Enable = BooleanType.是,
                    InitDate = DateTime.Now,
                    PasswordSalt = Utility.CreateSalt()
                };
                newCustomers.Password = Utility.GenerateHashWithSalt(account, newCustomers.PasswordSalt);
                _db.Customerses.Add(newCustomers);
                _db.SaveChanges();
                customers = newCustomers;
            }
            string userData = JsonConvert.SerializeObject(customers);
            Utility.SetAuthenTicket(userData, customers.Account);
            return Content("success");
        }

        public ActionResult LoginWithPassword(string account, string password)
        {
            var customers = _db.Customerses.FirstOrDefault(x => x.Account == account && x.Enable == BooleanType.是);
            if (customers == null)
            {
                return Content("登入失敗");
            }
            string hPassword = Utility.GenerateHashWithSalt(password, customers.PasswordSalt);
            string userData = "";
            if (!hPassword.Equals(customers.Password))
            {
                if (customers.Password.Equals(password))
                {
                    userData = JsonConvert.SerializeObject(customers);
                    Utility.SetAuthenTicket(userData, customers.Account);
                    return Content("success");
                }
                return Content("登入失敗");
            }

            userData = JsonConvert.SerializeObject(customers);
            Utility.SetAuthenTicket(userData, customers.Account);
            return Content("success");
        }

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="account">account</param>
        /// <param name="password">password</param>
        /// <param name="treeValid">gamil or fb account</param>
        /// <param name="ReturnUrl">Return Url</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string account, string password, string treeValid, string customerName, string customerEmail, string ReturnUrl)
        {

            if (!string.IsNullOrEmpty(treeValid))
            {
                account = treeValid;
            }
            var customers = _db.Customerses.FirstOrDefault(x => x.Account == account);
            if (customers == null)
            {
                if (!string.IsNullOrEmpty(treeValid))
                {
                    Customers newCustomers = new Customers
                    {
                        Account = account,
                        Email = customerEmail,
                        Name = customerName,
                        Enable = BooleanType.是,
                        InitDate = DateTime.Now,
                        PasswordSalt = Utility.CreateSalt()
                    };
                    newCustomers.Password = "FBGmail";
                    _db.Customerses.Add(newCustomers);
                    _db.SaveChanges();
                    customers = newCustomers;
                }
                else
                {
                    ViewBag.Message = "登入失敗";
                    return View();
                }

            }
            string userData = "";
            //正常註冊要檢查密碼
            if (string.IsNullOrEmpty(treeValid))
            {
                string hPassword = Utility.GenerateHashWithSalt(password, customers.PasswordSalt);
                if (!hPassword.Equals(customers.Password))
                {
                    if (customers.Password.Equals(password))
                    {
                        return AccountLogin(account, ReturnUrl, customers, out userData);
                    }
                    ViewBag.Message = "登入失敗";
                    return View();
                }
            }

            return AccountLogin(account, ReturnUrl, customers, out userData);



        }

        private ActionResult AccountLogin(string account, string ReturnUrl, Customers customers, out string userData)
        {
            //登入
            userData = JsonConvert.SerializeObject(customers);
            Utility.SetAuthenTicket(userData, customers.Account);

            if (string.IsNullOrEmpty(ReturnUrl))
            {
                ReturnUrl = "/Home/Index";
            }
            if (ReturnUrl.Contains("shoppingcar"))
            {
                ReturnUrl = "https://ec.fun2tw.com" + ReturnUrl;
            }
            ViewBag.ReturnUrl = ReturnUrl;
            ViewBag.account = account;
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterView Register)
        {
            if (ModelState.IsValid)
            {
                Google.reCAPTCHA reCaptcha = new reCAPTCHA("6Lf5cCITAAAAACnusCRm9jxe9dMpuddXjZTl8mzn", Request["g-recaptcha-response"]);
                bool isVaild = reCaptcha.verification();
                if (!isVaild)
                {
                    ViewBag.Message = "驗證失敗";
                    return View(Register);
                }
                var exCustomers = _db.Customerses.Where(x => x.Account == Register.Account);
                if (exCustomers.Any())
                {
                    ViewBag.Message = "帳號重複!";
                    return View(Register);
                }
                if (Register.Account.IndexOf('@') <= 0 && Register.Account.IndexOf('0') != 0)
                {
                    ViewBag.Message = "帳號格式錯誤!須為手機或E-mail";
                    return View(Register);
                }


                Customers customers = Register.GetCustomers();

                _db.Customerses.Add(customers);
                customers.Create(_db, _db.Customerses);

                //登入
                string userData = JsonConvert.SerializeObject(customers);
                Utility.SetAuthenTicket(userData, customers.Account);


                return RedirectToAction("RegisterSuccess");
            }

            return View(Register);
        }

        public ActionResult MobileRegister(string account, string password)
        {
            if (ModelState.IsValid)
            {


                var exCustomers = _db.Customerses.Where(x => x.Account == account);
                string userData = "";
                if (exCustomers.Any())
                {
                    //登入
                    userData = JsonConvert.SerializeObject(exCustomers);
                    Utility.SetAuthenTicket(userData, exCustomers.FirstOrDefault().Account);
                    return Content("帳號重複!");
                }
                //if (account.IndexOf('@') <= 0 && account.IndexOf('0') != 0)
                //{
                //    return Content("帳號格式錯誤!須為手機或E-mail");
                //}


                Customers customers = new Customers();
                customers.Account = account;
                string PasswordSalt = Utility.CreateSalt();
                string hPassword = Utility.GenerateHashWithSalt(password, PasswordSalt);
                customers.PasswordSalt = PasswordSalt;
                customers.Password = hPassword;
                _db.Customerses.Add(customers);
                customers.Create(_db, _db.Customerses);

                //登入
                userData = JsonConvert.SerializeObject(customers);
                Utility.SetAuthenTicket(userData, customers.Account);


                return Content("Success");
            }

            return Content("送入資料有誤");
        }


        public ActionResult MobileLogin(string account, string password)
        {
            if (ModelState.IsValid)
            {


                var customers = _db.Customerses.Where(x => x.Account == account);
                string userData = "";
                if (customers.Any())
                {
                    Customers customer = customers.FirstOrDefault();
                    string hPassword = Utility.GenerateHashWithSalt(password, customer.PasswordSalt);
                    if (!hPassword.Equals(customer.Password))
                    {
                        return Content("Error");

                    }
                    //登入
                    userData = JsonConvert.SerializeObject(customers);
                    Utility.SetAuthenTicket(userData, customer.Account);
                    return Content("Success");
                }
                return Content("Error");
            }

            return Content("送入資料有誤");
        }



        public ActionResult RegisterSuccess()
        {

            return View();
        }

        [Authorize]
        public ActionResult Order()
        {
            var customers = _db.Customerses.FirstOrDefault(x => x.Account == User.Identity.Name);
            if (customers == null)
            {

                return RedirectToAction("Index");
            }

            //var orders = customers.TravelOrders.Where(w => w.PayType == PayType.自動櫃員機ATM繳費 || (w.PayType == PayType.信用卡刷卡 && w.OrderStatus != OrderStatus.待處理));

            return View(customers);
        }
        [Authorize]
        public ActionResult TravelOrderDetail(int Id = 0)
        {
            var order = _db.TravelOrders.Find(Id);
            if (order == null)
            {

                return RedirectToAction("Order");
            }
            return View(order);
        }

        [Authorize]
        [HttpPost]
        public ActionResult TravelOrderDetail(string RemittanceName, string RemittanceAccount, int Id = 0)
        {
            var order = _db.TravelOrders.Find(Id);
            if (order == null)
            {

                return RedirectToAction("Order");
            }
            order.RemittanceAccount = RemittanceAccount;
            order.RemittanceName = RemittanceName;
            order.OrderStatus = TravelOrderStatus.通知已付款;

            _db.SaveChanges();
            //發送Email
            TravelProduct travelProduct = _db.TravelProducts.Find(order.SchduleId);
            MailTemplate mailTemplate = new MailTemplate();
            mailTemplate.MailSubject = "瘋趣台灣-ATM繳費確認通知";
            mailTemplate.SendATMCheckEmail(order, travelProduct, "fun2taiwan@gmail.com");


            return View(order);
        }


        [Authorize]
        public ActionResult Update()
        {
            var customers = _db.Customerses.FirstOrDefault(x => x.Account == User.Identity.Name);
            if (customers == null)
            {

                return RedirectToAction("Index");
            }
            RegisterView registerView = new RegisterView(customers);
            return View(registerView);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Update(Customers customers, string Opassword)
        {
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(Opassword))
                {
                    customers.Password = Utility.GenerateHashWithSalt(Opassword, customers.PasswordSalt);
                }
                _db.Entry(customers).State = EntityState.Modified;
                _db.SaveChanges();

                return RedirectToAction("UpdateSuccess");
            }
            return RedirectToAction("Update");
        }

        [Authorize]
        public ActionResult UpdateSuccess()
        {

            return View();
        }

        [Authorize]
        public ActionResult UpdateMovie()
        {

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult UpdateMovie(CustomerVideo customerVideo, HttpPostedFileBase upImageUrls)
        {
            if (upImageUrls != null)
            {
                if (upImageUrls.ContentType.ToLower().IndexOf("mp4", System.StringComparison.Ordinal) == -1)
                {
                    ViewBag.Message = "檔案型態錯誤!請上傳MP4 影音檔";
                    return View(customerVideo);
                }
            }
            //取得副檔名
            string extension = upImageUrls.FileName.Split('.')[upImageUrls.FileName.Split('.').Length - 1];
            //新檔案名稱
            string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, extension);
            string savedName = Path.Combine(Server.MapPath("~/VideoFiles"), fileName);
            upImageUrls.SaveAs(savedName);

            var customers = _db.Customerses.FirstOrDefault(x => x.Account == User.Identity.Name);
            if (customers == null)
            {

                return RedirectToAction("Index");
            }
            customerVideo.CustomerId = customers.Id;
            customerVideo.Enable = BooleanType.否;
            customerVideo.UpFile = fileName;
            customerVideo.InitDate = DateTime.Now;
            _db.CustomerVideos.Add(customerVideo);
            _db.SaveChanges();
            return RedirectToAction("UpdateMovieSuccess");
        }

        [Authorize]
        public ActionResult UpdateMovieSuccess()
        {

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult UpdateMovieFtp(String Account, String Subject, String Memo, String FileName)
        {
            CustomerVideo customerVideo = new CustomerVideo();
            var customers = _db.Customerses.FirstOrDefault(x => x.Account == Account);
            if (customers == null)
            {
                return Content("會員帳號不存在");
            }
            customerVideo.CustomerId = customers.Id;
            customerVideo.Subject = Subject;
            customerVideo.Memo = Memo;
            customerVideo.UpFile = FileName;
            customerVideo.Enable = BooleanType.否;
            customerVideo.InitDate = DateTime.Now;

            _db.CustomerVideos.Add(customerVideo);
            _db.SaveChanges();

            return Content("success");
        }

        public ActionResult ForgetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgetPassword(String account)
        {
            Customers customer = _db.Customerses.FirstOrDefault(f => f.Account == account.Trim());
            if (customer != null)
            {
                String password = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 6);
                customer.Password = Utility.GenerateHashWithSalt(password, customer.PasswordSalt);
                _db.SaveChanges();

                //String mailSubject = "瘋趣台灣網站系統通知";
                //String mailBody = "<p>親愛的 瘋趣台灣 網站會員您好：</p>" +
                //    "<p>您是否忘記您在瘋趣台灣網站的會員帳號資訊了呢？<br />瘋趣台灣網站收到您的密碼重設要求，故發出這封「忘記密碼通知」信函。</p>" +
                //    "<p>為保護您的資料，以下提供您新的會員密碼，請您務必儘快登入<a href ='https://www.fun2tw.com/Customer'>瘋趣台灣</a>網站，至 修改個人資料 重新設定您的密碼，謝謝！</p>" +
                //    "<p>帳號: " + customer.Account + "<br />密碼: " + password + "<br />================================================</p>" +
                //    "本電子郵件之內容及其附件係由 瘋趣台灣網站所傳送，所含之資料僅供指定之收件人使用。<br />" +
                //    "除了本電子郵件所指定之收件人外，任何人或公司不得就本電子郵件全部或部分之內容為閱讀、複製、轉寄、公開、保存或為其他使用。<br />" +
                //    "若您非指定之收件人，請立即通知瘋趣台灣網站客服，並完全刪除本電子郵件，謝謝您的合作。";

                ////Utility.SystemSendMail(customer.Account, mailSubject, mailBody);
                //Utility.SendGmailMail(ConfigurationManager.AppSettings["MailFrom"], customer.Account, mailSubject, mailBody, "fun2tw2016");

                //發送Email

                MailTemplate mailTemplate = new MailTemplate();
                mailTemplate.SendForgetPasswordEmail(customer.Account, password, customer.Email);

                @ViewBag.Message = "密碼已重設";
                ViewBag.ResetPassword = true;

            }
            else
            {
                @ViewBag.Message = "找不到符合的帳號";
                ViewBag.ResetPassword = false;
            }
            return View();
        }



        public ActionResult LogOff()
        {
            System.Web.Security.FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult PortalOrder()
        {


            return View();
        }
    }
}
