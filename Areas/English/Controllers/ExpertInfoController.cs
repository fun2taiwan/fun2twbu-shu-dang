﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2twModels.Models;
using Newtonsoft.Json;

namespace Fun2tw.Areas.English.Controllers
{
    public class ExpertInfoController : Controller
    {
        private BackendContext _db = new BackendContext();
        //
        // GET: /ExpertInfo/

        public ActionResult Index()
        {
            var travelPlan = _db.TravelPlans.Where(x=>x.TravelPlanImages.Count>0).OrderByDescending(x => x.Views).Take(4).ToList();
            ViewBag.travelPlan = travelPlan;
            var ExpertInfo = _db.ExpertInfos.OrderBy(x => x.ListNum).Take(3).ToList();
            return View(ExpertInfo);
        }

        public ActionResult Details(int id = 0)
        {
            ExpertInfo expertinfo = _db.ExpertInfos.Find(id);
            if (expertinfo == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(expertinfo);
        }

        public ActionResult Plan()
        {
            var travelOldPlan = _db.TravelOldPlans.OrderBy(x => x.InitDate).ToList();
            return View(travelOldPlan);
        }

        public ActionResult Json(int id = 0)
        {

            string jsonContent;
            var travelOldPlans = _db.TravelOldPlans.OrderByDescending(p => p.InitDate).Select(
                    c => new
                    {
                        Id = c.Id,
                        Subject = c.Subject,
                        UpImageUrl = "https://www.fun2tw.com/upfiles/images/" + c.UpImageUrl,
                        InitDate = c.InitDate
                    });
            jsonContent = JsonConvert.SerializeObject(travelOldPlans, Formatting.Indented);

            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }

        public ActionResult PlanDetails(int id = 0)
        {
            TravelOldPlan travelOldPlan = _db.TravelOldPlans.Find(id);
            if (travelOldPlan == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(travelOldPlan);
        }


    }
}
