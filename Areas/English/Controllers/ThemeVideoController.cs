﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2tw.Controllers;
using Fun2twModels.Models;
using MvcPaging;
using Newtonsoft.Json;

namespace Fun2tw.Areas.English.Controllers
{
    public class ThemeVideoController :_BaseController
    {
        //
        // GET: /ThemeVideo/
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 20;

        public ActionResult Index(int? page, FormCollection fc)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);

            return View(_db.ThemeVideos.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));
        }

        //
        // GET: /TravelProduct/Details/5

        public ActionResult Details(int id = 0)
        {
            ThemeVideo themevideo = _db.ThemeVideos.Find(id);
            if (themevideo == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(themevideo);
        }

     

    }
}
