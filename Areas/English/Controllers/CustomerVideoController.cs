﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2tw.Controllers;
using Fun2twModels.Models;
using MvcPaging;
using Newtonsoft.Json;

namespace Fun2tw.Areas.English.Controllers
{
    public class CustomerVideoController : _BaseController
    {
        //
        // GET: /ThemeVideo/
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 20;

        public ActionResult Index(int? page, FormCollection fc)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);

            return View(_db.CustomerVideos.Where(x=>x.Enable==BooleanType.是).OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));
        }

         [Authorize]
        public ActionResult MyVideo(int? page, FormCollection fc)
        {
            var customers = _db.Customerses.FirstOrDefault(x => x.Account == User.Identity.Name);
            if (customers == null)
            {

                return RedirectToAction("Index");
            }
            return View(customers);
        }

        //
        // GET: /TravelProduct/Details/5

        public ActionResult Details(int id = 0)
        {
            CustomerVideo customerVideo = _db.CustomerVideos.Find(id);
            if (customerVideo == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(customerVideo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteVideo(int id)
        {
            CustomerVideo Video = _db.CustomerVideos.Find(id);
            _db.CustomerVideos.Remove(Video);
            _db.SaveChanges();
            return RedirectToAction("MyVideo");
        }

        [AllowAnonymous]
        public ActionResult GetJson()
        {
            var item = _db.CustomerVideos.Where(x => x.Enable == BooleanType.是)
                .Select(
                c => new
                {
                    Id = c.Id,
                    Subject = c.Subject,
                    UpImageUrl = c.UpImageUrl.Replace("http://","https://"),
                    YoutubeUrl = c.YoutubeUrl,
                    YoutubeEmbedUrl = c.YoutubeUrl.Replace("watch?v=", "embed/"),

                    InitDate = c.InitDate
                }
                ).OrderByDescending(o => o.InitDate);
            string jsonContent;
            if (item.ToList().Count == 0)
            {
                CustomerVideo pm = new CustomerVideo
                {
                    Id = 0,
                    Subject = "目前無資料"
                };
                List<CustomerVideo> list = new List<CustomerVideo> { pm };
                jsonContent = JsonConvert.SerializeObject(list, Formatting.Indented);
            }
            else
            {
                jsonContent = JsonConvert.SerializeObject(item.ToList(), Formatting.Indented);
            }
            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }

    }
}
