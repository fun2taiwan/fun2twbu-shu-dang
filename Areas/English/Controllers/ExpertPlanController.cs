﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2tw.Controllers;
using Fun2twModels.Models;
using MvcPaging;
using Newtonsoft.Json;

namespace Fun2tw.Areas.English.Controllers
{
    public class ExpertPlanController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //
        // GET: /ExpertPlan/

        public ActionResult Index(int? page, FormCollection fc)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            var ExpertInfo = _db.ExpertInfos.OrderBy(x => x.ListNum).Take(10).ToList();
            ViewBag.ExpertInfo = ExpertInfo;

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);

            return View(_db.ExpertPlans.Where(x=>x.UpImageUrl!=null).OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));
        }

        public ActionResult Details(int id = 0)
        {
            ExpertPlan expertplan = _db.ExpertPlans.Find(id);
            if (expertplan == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(expertplan);
        }

        public ActionResult Json(int id = 0)
        {

            string jsonContent;
            var travelplans = _db.ExpertPlans.OrderByDescending(p => p.InitDate).Select(
                    c => new
                    {
                        Id = c.Id,
                        ExpertName = c.ExpertInfo.Subject,
                        Subject = c.Subject,
                        UpImageUrl =  "https://www.fun2tw.com/upfiles/images/" + c.UpImageUrl,
                        InitDate = c.InitDate
                    });
            jsonContent = JsonConvert.SerializeObject(travelplans, Formatting.Indented);



            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }

    }
}
