﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2tw.Models;
using Fun2twModels.Models;
using Google;

namespace Fun2tw.Areas.Japan.Controllers
{
    public class ContactUSController : Controller
    {
        private BackendContext _db = new BackendContext();
        //
        // GET: /ContactUS/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Index(ContactUS contactus)
        {
            if (ModelState.IsValid)
            {
                Google.reCAPTCHA reCaptcha = new reCAPTCHA("6Lf5cCITAAAAACnusCRm9jxe9dMpuddXjZTl8mzn", Request["g-recaptcha-response"]);
                bool isVaild = reCaptcha.verification();
                if (!isVaild)
                {
                    ViewBag.Message = "驗證失敗";
                    return View(contactus);
                }
                contactus.InitDate=DateTime.Now;
                _db.ContactUss.Add(contactus);
                _db.SaveChanges();
                //發送Email
              
                MailTemplate mailTemplate = new MailTemplate();
                mailTemplate.MailSubject = "瘋趣台灣-聯絡我們通知";
                mailTemplate.SendContactUSEmail(contactus, "fun2taiwan@gmail.com");
                return RedirectToAction("Success", "ContactUS", new { Mobile = Request["Mobile"] });
            }

            return View(contactus);
        }

        public ActionResult Success()
        {
            return View();
        }

    }
}
