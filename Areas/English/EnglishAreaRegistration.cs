﻿using System.Web.Mvc;

namespace Fun2tw.Areas.English
{
    public class EnglishAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "English";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "English_default",
                "English/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
