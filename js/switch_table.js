//時刻表
$(function(){
  $('#timeTableHoliday').hide();
  $('#timeTableWeekdayButton').click(function(event) {
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    $('#timeTableWeekday').fadeIn();
    $('#timeTableHoliday').hide();
  });
  $('#timeTableHolidayButton').click(function(event) {
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    $('#timeTableWeekday').hide();
    $('#timeTableHoliday').fadeIn();
  });
})