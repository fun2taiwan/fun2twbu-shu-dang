//Kyo JavaScript Document
$(function(){
  // data
  var data = {
    num_now:0,
    num_total:4,
    score:0,
    question:[
    {
      content:'1.請欣賞完影片後回答哪一項不是瘋趣台灣app所提供的服務？',
      answer:'C',
      choise: ['預約墾丁快線班次','中途站點自由上下車','天氣預報']
    },
    {
      content:'2.哪一個地區墾丁快線不會停靠呢？',
      answer:'B',
      choise: ['左營高鐵','仁武','南灣']
    },
    {
      content:'3.海生館內不會有哪種生物呢？',
      answer:'A',
      choise: ['羊駝','小白鯨','小丑魚']
    },
    {
      content:'4.哪個地區不適合從事浮潛活動呢？',
      answer:'C',
      choise: ['萬里桐','後壁湖','南仁湖']
    },
    {
      content:'5.下列哪部電影是在恆春半島取景的呢？',
      answer:'B',
      choise: ['海角六號','海角七號','海角八號']
    },

    ]
  };
  //start
  $('.start a').on('click', function(event) {
    event.preventDefault();
    /* Act on the event */
    $('.start').hide();
    $('.header').addClass('active');
    $('.game').show();
    question();$('.jq-video').show();
  });
  // 題庫
  function question(){
    $('.question').text(data.question[data.num_now].content);
    $.each($('.choise li span'), function(index, val) {
       /* iterate through array or object */
       $(this).text(data.question[data.num_now].choise[index]);
    });
  }
  
  // next
  $('.next a').click(function(event) {
    /* Act on the event */
    event.preventDefault();
    next();
  });
  function next(){
    $('.jq-video').remove();
    var answer = data.question[data.num_now].answer;
    var feedback = $('.choise input:checked').val();
    if(!feedback){
      alert('您尚無答題!');
      return false;
    }
    if(answer == feedback){
      data.score+=1;
    }
    if(data.num_now!=data.num_total){
      $('.choise input').attr("checked",false);  
      data.num_now+=1;
      question();
    }else{
      $('.game').hide();
      if(data.score==5){
        $(".success").show();
      }
      else{
        $('.boldRed').text(data.score);
        $('.fail').show();
      }
    }
  }
  function refresh(){
     location.reload();
  }
  //refresh
  $('.fail a,.fails').click(function(event) {
    event.preventDefault();
     location.reload();
  });
});
